# Semester program
This program is intended to simulate cars going from their start point to ending point through roads. There is flexible part where parameters can be set to observe how the program behaves. 

Cars are generated randomly with the information of starting point, ending point and the time they are generated. Each car has assigned path from start point to end point. The path is calculated with Dijkstra algorithm that finds shortest path from start to finish. More about Dijktra's algorithm can be found in Průvodce labyrintem algoritmů (M. Mareš ,T. Valla).

**Program**
Main data strictures used are queues and lists. Queues serve naturally for "picking the one that waits the longest". As cars are allocated on roads, it is important for them to follow order. Lists are used for storage of events that will take place (not necessarily ordered by time).
Cars as objects are moving through roads and they are regulated by crossroads's semaphores. Each process has set of instructions for events (they are InicializeCar,Start, Availability, AddedOnRoad, LeaveRoad, SwitchSemaphore). 


_Road processes:_

     1. Start - where it lets cars that are on road queue continue in their path, 
     2. Availability - road decides whether the car which is asking to arrive on the road (waiting in imaginary waiting line) 
     has space on road to be allocated to, and if yes then plans the car the time of arrival
     3. AddedOnRoad - car is added into final road queue, where it waits to be called by the road's start event
_Car processes:_

     1. InicializeCar - this unique event is only planned at the beginning of the car's existence, adds car into waiting line of the starting road
     2. Start - Car has Start called upon itself when it is ready to leave the road it is currently on and is added into waiting line of next
     road in its path list. Also, total waiting time is increased by a time difference car has between the recorded time of stoping vs. now.
     3. LeaveRoad - Car leaves the road is is coming from. With its leaving it frees space on the previous road. 
     If there are no more roads to take, car reached its goal destination.
_Crossroad processes:_

    1. SwitchSemaphore - where it shifts the address of road that has 'green light' to the next one in list.
    2. Start - this event is called (mainly) by roads which belong to the crossroad and means that start for the 
     road that has green light should be called


**Input data file and output**

- [ ] Input

There are 3 main sections for input.

    - Destination 
    - Crossroad
    - Road
Each of them should be written on separate line start with upper letter of the section 
_(example Car starts with C, Crossroad - C, for more see attached input file)_. For there sections to be readable by program it is necessary to add blank space between properties. The properties differ between sections. The processes have to contain description followed after dash.

    - Destination - id
    - Crossroad - id
    - Road - id, destination(from), destination(to), road length.

Input is case sensitive and depends on order of the specified properties. Destinations and crossroads must be described before specification of roads. When the set of roads represents graph that is not oriented, then each road needs to be written explicitly from both directions. (See attached input file for how the input should look).
The input file should be placed in the same folder and computer address as the source code.

When program is started, three adjustable numbers can be set. Semaphor interval, number of cars and the time in which the cars should be generated. By clicking button compute the simulation initiates and starts computation.

- [ ] Output

 - is a single number representing average of 10 iterations of program with set properties. Rounded to 2 decimal numbers


**Course of the project development**

Development of this project was affected by not the finest event design. I feel that there must be more decent way to execute whole process with different events. Program lacks loose connection and is very inconvenient to continue developing on further features. 
With the actual design there is also possible touble with generation of cars with differing sizes. Might happen that car "outruns" another car that is in front of him because car, second in road queue, is smaller and therefore enter the road. 
What would certainly be interesting is watching how the performance would change with different algorithm of semaphore and multiple-lane roads.
